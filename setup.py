#
# Copyright 2019 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
# These materials are licensed under the Amazon Software License in connection with the Alexa Gadgets Program.
# The Agreement is available at https://aws.amazon.com/asl/.
# See the Agreement for the specific terms and conditions of the Agreement.
# Capitalized terms not defined in this file have the meanings given to them in the Agreement.
#
from setuptools import setup

setup(
    name='alexa_gadget',
    version='0.1.0',
    packages=['alexa_gadget'],
    package_dir={'': 'src'},
    install_requires=[
        'pybluez', 'protobuf', 'python-dateutil', 'gpiozero', 'colorzero'
    ],
    license='Apache 2'
)
