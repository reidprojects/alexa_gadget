import json
import subprocess
import os
import dbus
from dbus.mainloop.glib import DBusGMainLoop

from alexa_gadget.gadget import BLE, BT, _TRANSPORT_MODE, _ECHO_BLUETOOTH_ADDRESS
from alexa_gadget.base_adapter import BaseAdapter


def pair(config_file_path):
    switch_transport_mode = False

    transport_mode = None
    echo_bluetooth_address = None

    first_time_setup = os.path.exists(config_file_path)

    # if setup being run for the first time, let user choose the transport mode
    if first_time_setup:
        user_input = input("Which transport mode would you like to configure your gadget for (ble/bt)?").strip()
        while user_input.lower() not in [BLE.lower(), BT.lower()]:
            user_input = input(
                "Invalid choice!\nWhich transport mode would you like to configure your gadget for (ble/bt)?").strip()
        switch_transport_to = BLE if user_input.lower() == BLE.lower() else BT

    # else, let user switch the transport mode
    else:
        try:
            # determine the currently configured transport mode
            with open(config_file_path, "r") as read_file:
                data = json.load(read_file)
                transport_mode = data.get(_TRANSPORT_MODE, None)
                echo_bluetooth_address = data.get(_ECHO_BLUETOOTH_ADDRESS, None)
            # if transport mode not configured correctly in the config file, raise exception which would be caught and
            # user will be asked to re-select the transport mode
            if transport_mode not in [BLE, BT]:
                raise Exception
            switch_transport_to = BT if transport_mode == BLE else BLE
            switch_transport_mode = True if input(
                "Your gadget is currently configured to use {} transport mode.\n"
                "Do you want to switch to {} transport mode (y/n)? "
                    .format(transport_mode, switch_transport_to)).strip().lower() == 'y' else False
        except:
            print("Invalid transport mode found in config file!")
            user_input = input("Which transport mode would you like to configure your gadget for (ble/bt)?").strip()
            while user_input.lower() not in [BLE.lower(), BT.lower()]:
                user_input = input(
                    "Invalid choice!\n"
                    "Which transport mode would you like to configure your gadget for (ble/bt)?").strip()
            switch_transport_to = BLE if user_input.lower() == BLE.lower() else BT
            switch_transport_mode = True

    if first_time_setup or switch_transport_mode:
        if switch_transport_mode:
            # first unpair gadget from Echo device
            print("While switching the transport mode, gadget needs to be unpaired from the Echo device.\n" +
                  "Please unpair the gadget from the Echo device using the Bluetooth menu in Alexa App or Echo\'s screen.\n")

            # prompt the user to unpair the gadget from the Echo device
            input("Press ENTER to continue once you've unpaired your gadget from the Echo device.")

            print("Clearing pairing bond from the gadget...")
            # create dummy adapter and use its unpair functions
            try:
                BaseAdapter(dbus.SystemBus(DBusGMainLoop()), dbus).unpair(echo_bluetooth_address)
            except Exception:
                pass

            # remove the Echo device's bt address from the config
            with open(config_file_path, "w+") as write_file:
                write_data = {_ECHO_BLUETOOTH_ADDRESS: None, _TRANSPORT_MODE: transport_mode}
                json.dump(write_data, write_file)

        # put BlueZ in compatibility mode if it isn't already
        subprocess.run(
            'sudo sed -i "s/bluetoothd$/bluetoothd --compat/" /etc/systemd/system/bluetooth.target.wants/bluetooth.service',
            shell=True)

        # add user to 'bluetooth' group
        subprocess.run('sudo usermod -a -G "bluetooth" "$USER"', shell=True)

        # restart Bluetooth daemon
        subprocess.run('sudo systemctl daemon-reload; sudo systemctl restart bluetooth', shell=True)

        # store the transport mode in the config file
        data = {}
        if not first_time_setup:
            with open(config_file_path, "r") as read_file:
                data = json.load(read_file)
        with open(config_file_path, "w+") as write_file:
            data[_TRANSPORT_MODE] = switch_transport_to
            json.dump(data, write_file)


    print("+------------------------------+")
    print("|            SUCCESS           |")
    print("+------------------------------+\n")
